FROM node:14.0.0
RUN mkdir /web-blog
COPY package.json package-lock.json /web-blog/
WORKDIR /web-blog
RUN npm install
COPY . /web-blog
RUN ls
ARG myenv
RUN npm run build:$myenv
