# estágio de compilação
FROM node:14.15.1 as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
ARG myenv
RUN npm run build:$myenv


FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
EXPOSE 8001
CMD ["nginx", "-g", "daemon off;"]
