import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    MetaMaskAddress: '',
    Pending: [],
    NetworkId: null,
    Lan:'',
    IsPhone:false
  },
  mutations: {
    setMetaMaskAddress(state, val) {
      state.MetaMaskAddress = val
    },
    setPending(state, val) {
      state.Pending = val
    },
    setNetworkId(state, val) {
      state.NetworkId = val
    },
    setLan(state, val) {
      state.Lan = val
    },
    setIsPhone(state, val) {
      state.IsPhone = val
    },
  },
  actions: {},
  modules: {},
})
