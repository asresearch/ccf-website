import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App.vue'
import router from './router'
import store from './store'
// import Parse from './parse'
import i18n from './lang'
import { ntf } from './components/notify'

Vue.use(ElementUI)
// Vue.use(Parse)
Vue.mixin(ntf)
Vue.config.productionTip = false
new Vue({
	router,
	store,
	i18n,
	render: (h) => h(App)
}).$mount('#app')
