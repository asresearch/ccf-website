const ntf = {
  methods: {
    Success(msg,title=this.$t('yesno.success'), offset = 80, duration = 0) {
      this.$notify({
        type: 'success',
        title: title,
        message: msg,
        offset: offset,
        duration: duration
      });
    },
    Warning(msg,title=this.$t('yesno.warning'), offset = 80, duration = 0) {
      this.$notify({
        type: 'warning',
        title: title,
        message: msg,
        offset: offset,
        duration: duration
      });
    },
    Error(msg,title=this.$t('yesno.error'),  offset = 80, duration = 0) {
      this.$notify({
        type: 'error',
        title: title,
        message: msg,
        offset: offset,
        duration: duration
      });
    }
  }
}
export { ntf }
