import Vue from 'vue'
// 引入并使用vue-i18n插件
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

import zh from './zh'
import en from './en'

const i18n = new VueI18n({
	// locale:localStorage.getItem('cffLocale')?localStorage.getItem('cffLocale'):window.navigator.language==='zh-CN'?'zh':'en' , // 语言标识
	locale: 'en',
	messages: {
		zh: zh,
		en: en
	}
})
export default i18n
