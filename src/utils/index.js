import dayjs from 'dayjs'

const milliseconds = function(time) {
  if (time) {
    const t = parseInt(time / 60 / 60 / 24)
    const h = parseInt(time / 60 / 60 - t * 24)
    const m = parseInt(time / 60 - h * 60 - t * 24 * 60)
    // const s = parseInt(time / 1000 - h * 60 * 60 - m * 60-t*24*60)
    return t + 'd  ' + h + 'h  ' + m + 'min  ' + 'left'
  }
  return 'in progress'
}
//转换数据格式
const toHexString = function(bytes) {
  return bytes.reduce(
    (str, byte) => str + byte.toString(16).padStart(2, '0'),
    ''
  )
}
//生成随机字符串
const randomString = function(e) {
  e = e || 32
  var t = 'ABCDEFGHIJKLMNOPQRSTUVWSYZabcdefghijklmnopqrstuvwsyz123456789',
    a = t.length,
    n = ''
  for (let i = 0; i < e; i++) {
    n += t.charAt(Math.floor(Math.random() * a))
  }
  return n
}
const timeFormatting = function (time, isPhone=false) {
  const times = isPhone?dayjs(time).format('MM-DD HH:mm:ss'):dayjs(time).format('YYYY-MM-DD HH:mm:ss')
  return times
}

const copys = function(thit, metaMaskAddress) {
  const spanText = metaMaskAddress
  const oInput = document.createElement('input')
  oInput.value = spanText
  document.body.appendChild(oInput)
  oInput.select() // 选择对象
  document.execCommand('Copy') // 执行浏览器复制命令
  oInput.className = 'oInput'
  oInput.style.display = 'none'
  document.body.removeChild(oInput)
}
export { milliseconds, randomString, toHexString, timeFormatting, copys }
