let API_URL = ''
let Network = ''
let contract = {}
let Tag = null
let ADAPTER = ''
let CURVEFI = ''
//Pdnding清除时间 毫秒值。
const PENDINGNUM = 21600000
//blocknative 库配置
const Wallets = [
	{
		walletName: 'metamask'
	},
	{
		walletName: 'walletConnect',
		infuraKey: 'd3818b1cddd14cf489c8d55b4ec69f92'
	}
]
const DappId = '84394960-6ede-462b-936c-bc71846be78d'

switch (process.env.NODE_ENV) {
	// 测试环境
	case 'test':
		Tag = '8.19'
		CURVEFI = '0x62869F49ea8b6c3EEdEcA8b8b1c6731090aD7A3D'
		ADAPTER = '0xF85999731C696d1C86558b057101b69889b41Fe8'
		API_URL = 'https://test-api.earning.farm'
		Network = 'ropsten'
		contract = {
			USDC: {
				CFToken_trust_list: '0xfEc241BC0982d2A7e5b89F4fAB0502e76F96E05f',
				CFToken: '0xF5FC4B459a00fb926F60F926F8C454Ed39134c09',
				ERC20DepositApprover: '0x6BEcEFb45cb2DC6013b6f0EB68AEad4390f1F548',
				CFVault: '0xece7949dC9Ec6eCD8aC65cb240D9bD1b6508cF1d',
				CFController: '0x1390F0e1329012C9378d26e00E4e06f2e88715e3',
				USDC: '0xC328F49Bc83e968EB6EF858dc9953DeB7fbeF9cF',
				CRV: '0xE4793084d77d430f1c9C37f31Bc0377c38aCd9a3',
				Decimal: 1e6,
				Name: '稳定币去中心化收益基金',
				Introduce:
					'基于Curve的多个稳定币池进行投资，每日收到的CRV奖励将转换成相应资产后继续投入。'
			},
			WBTC: {
				CFToken_trust_list: '0xc23C31Af8105B09D46AF8C1c9f24B15Fc518d3FF',
				CFToken: '0x9B872A869cE87A08dbe5c24b4c3E3d7E96560F69',
				ERC20DepositApprover: '0x6BEcEFb45cb2DC6013b6f0EB68AEad4390f1F548',
				CFVault: '0xE935b43f304f12851C1f9492bEE4DD37B327E32f',
				CFController: '0x4b583DFB8C7614eFdA8934205dC819B78782A72F',
				WBTC: '0xb28927e456c16D876Df02F1bbbEcE8BB74067CCE',
				CRV: '0xE4793084d77d430f1c9C37f31Bc0377c38aCd9a3',
				Decimal: 1e8,
				Name: 'BTC去中心化收益基金',
				Introduce:
					'基于Curve的多个BTC ERC20池进行投资，每日收到的CRV奖励将转换成相应资产后继续投入。'
			},
			ETH: {
				CFToken_trust_list: '0x745d97Eb8c714ac839d3dD505C996A3dFA27B476',
				CFToken: '0xA709eCF2253B18A757214D64F42026Be8F008bD8',
				CFVault: '0xa5eafc384f22d743EDfFa1aE5375bac95495fE2e',
				CRV: '0xD533a949740bb3306d119CC777fa900bA034cd52',
				Decimal: 1e18,
				Name: 'ETH去中心化收益基金',
				Introduce:
					'基于Curve的多个ETH池进行投资，每日收到的CRV奖励将转换成相应资产后继续投入。'
			}
		}
		break

	// 生产环境
	case 'production':
		CURVEFI = '0x93054188d876f558f4a66B2EF1d97d16eDf0895B'
		ADAPTER = '0x73aB2Bd10aD10F7174a1AD5AFAe3ce3D991C5047'
		API_URL = 'https://api-cff.earning.farm'
		Network = 'mainnet'
		contract = {
			USDC: {
				CFToken_trust_list: '0x246e50a0161b005AC9DD0AE80661C5ae843B64c3',
				CFToken: '0x44B439f6624e84c29f2ee5fd3C2CCf29C8DC7572',
				ERC20DepositApprover: '0xe5afC078684683dc232E053c2c9D86015Aa00Ec6',
				CFVault: '0xa1E263225E24333CA4d26083C94092D6bfEe1DDd',
				CFController: '0x6c57eA7e3B81b8a166ED5b8E396a401C7a9c890b',
				USDC: '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48',
				CRV: '0xD533a949740bb3306d119CC777fa900bA034cd52',
				Decimal: 1e6,
				Name: '稳定币去中心化收益基金',
				Introduce:
					'基于Curve的多个稳定币池进行投资，每日收到的CRV奖励将转换成相应资产后继续投入。'
			},
			WBTC: {
				CFToken_trust_list: '0xb9a5263108F14EF5021047a82852f473c2Dc400f',
				CFToken: '0x0319180cA78edB186fBFC7786E0E5f51FDF21263',
				ERC20DepositApprover: '0xe5afC078684683dc232E053c2c9D86015Aa00Ec6',
				CFVault: '0x1C62D47Aae452877D4E4ff6D7ECDaE5A50ef7326',
				CFController: '0xeb792a00F482DD2f75702dE9eFbC1C2C72e54a2E',
				WBTC: '0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599',
				CRV: '0xD533a949740bb3306d119CC777fa900bA034cd52',
				Decimal: 1e8,
				Name: 'BTC去中心化收益基金',
				Introduce:
					'基于Curve的多个BTC ERC20池进行投资，每日收到的CRV奖励将转换成相应资产后继续投入。'
			},
			ETH: {
				CFToken_trust_list: '0x745d97Eb8c714ac839d3dD505C996A3dFA27B476',
				CFToken: '0xA709eCF2253B18A757214D64F42026Be8F008bD8',
				CFVault: '0xE303a8Cc37C96669C7Ba5aeE1134bb530e766BdF',
				CRV: '0xD533a949740bb3306d119CC777fa900bA034cd52',
				Decimal: 1e18,
				Name: 'ETH去中心化收益基金',
				Introduce:
					'基于Curve的多个ETH池进行投资，每日收到的CRV奖励将转换成相应资产后继续投入。'
			}
		}
		break
}

export {
	Network,
	API_URL,
	contract,
	Tag,
	ADAPTER,
	CURVEFI,
	PENDINGNUM,
	Wallets,
	DappId
}
