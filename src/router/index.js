import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {path: '/', redirect: '/index'},
  {
    path: '/index',
    name: 'index',
    component: () => import('../views/Index.vue'),
    meta: {
      title: '主页',
      // requireAuth: true,  // 添加该字段，表示进入这个路由是需要登录的
    },
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    children: [
      {
        path: '/',
        redirect: '/cff',
      },
      {
        path: '/cff',
        name: 'cff',
        component: () => import('../views/components/Cff.vue'),
        meta: {
          title: '列表',
        },
      },

      {
        path: '/btcWbtc',
        name: 'btcWbtc',
        component: () => import('../views/components/BtcWbtc.vue'),
        meta: {
          title: '兑换',
        },
      },
    ],
  },
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes,
})

export default router
