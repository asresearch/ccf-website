import { contract, ADAPTER, CURVEFI } from '../config.js'
const ERC20Base_abi = require('./ERC20Base_abi.json')
const CFVault_abi = require('./CFVault_abi.json')
const CFController_abi = require('./CFController_abi.json')
const ERC20DepositApprover_abi = require('./ERC20DepositApprover_abi.json')
const IUSDCPool_abi = require('./IUSDCPool_abi.json')
const IERC20_abi = require('./IERC20_abi.json')
const CFETHVault_abi = require('./CFETHVault_abi.json')
const adapterCurveABI = require('./adapterCurveABI.json')
const CurveFi_abi = require('./CurveFi_abi.json')

const MintThenSwap = function(
  accounts,
  minExchangeRate,
  newMinExchangeRate,
  slippage,
  wbtcDestination,
  amount,
  nHash,
  sig
) {
  let data = getWeb3(adapterCurveABI, ADAPTER)
    .methods.mintThenSwap(
      minExchangeRate,
      newMinExchangeRate,
      slippage,
      wbtcDestination,
      amount,
      nHash,
      sig
    )
    .encodeABI()
    return {
      from: accounts,
      to: ADAPTER,
      value: 0,
      data: data,
    }
}
// 动态获取web3实例对象
const getWeb3 = function(abi, accounts) {
  let instance = new window.web3.eth.Contract(abi, accounts)
  return instance
}
//获取cur get_dy
const getDy = function() {
  return getWeb3(CurveFi_abi, CURVEFI)
    .methods.get_dy(0, 1, 100000000)
    .call()
}
//获取ETH
const getBalance = function(metaMaskAddress) {
  return new window.web3.eth.getBalance(metaMaskAddress)
}
const getPadding = function(hashString) {
  return new window.web3.eth.getTransactionReceipt(hashString)
}
// 获取CFVault实例对象
const getCFVault = function(CFVault) {
  return getWeb3(CFVault_abi, CFVault)
}

const getGoundage = async function(code) {
  const base = await getCFVault(contract[code].CFVault).methods.ratio_base().call() 
  const ratio =await getCFVault(contract[code].CFVault).methods.withdraw_fee_ratio ().call() 
   return (ratio/base *100) + '%'
}

const getHarvestFeeRatio = async function(code) {
  const cll = await getController(contract[code].CFVault)
  const cfc =await  getWeb3(CFController_abi, cll)
  const base = await cfc.methods.ratio_base().call() 
  const ratio =await cfc.methods.harvest_fee_ratio().call() 
   return (ratio/base *100) + '%'
}
//获取最大存储数量max_amount
const getMaxAMount = function(code) {
  return getCFVault(contract[code].CFVault)
    .methods.max_amount()
    .call()
}
//获取IERC20地址
const getIERC = function(CFVault) {
  return getCFVault(CFVault)
    .methods.target_token()
    .call()
}
//存ETH
const setDepositETH = function(code) {
  return getWeb3(CFETHVault_abi, contract[code].CFVault)
    .methods.deposit()
    .encodeABI()
}

//取ETH
const getWithdrawETH = function(number, code) {
  return getWithdraw(number, code)
}
//存
const setDeposit = function(number, code) {
  return getWeb3(ERC20DepositApprover_abi, contract[code].ERC20DepositApprover)
    .methods.deposit(
      contract[code][code],
      number,
      contract[code].CFVault,
      contract[code].CFToken
    )
    .encodeABI()
}
//取
const getWithdraw = function(number, code) {
  return getCFVault(contract[code].CFVault)
    .methods.withdraw(number)
    .encodeABI()
}
//获取IERC20BalanceOf
const getIERCBalanceOf = async function(accounts, code) {
  let ierc = await getIERC(contract[code].CFVault)
  return getWeb3(IERC20_abi, ierc)
    .methods.balanceOf(accounts)
    .call()
}
//查询授权额度
const getAllowance = async function(accounts, code) {
  let ierc = await getIERC(contract[code].CFVault)
  return getWeb3(IERC20_abi, ierc)
    .methods.allowance(accounts, contract[code].ERC20DepositApprover)
    .call()
}
//授权
const setApprove = async function(number, accounts, code) {
  let ierc = await getIERC(contract[code].CFVault)
  return getWeb3(IERC20_abi, ierc)
    .methods.approve(contract[code].ERC20DepositApprover, number)
    .send({
      from: accounts,
    })
}
//获取CFv余额地址
const getCFVBalanceOf = function(address, code) {
  return getWeb3(IERC20_abi, contract[code].CFToken)
    .methods.balanceOf(address)
    .call()
}
//获取cfname
const getCFTName = function(code) {
  return getWeb3(ERC20Base_abi, contract[code].CFToken)
    .methods.name()
    .call()
}
//获取CFv地址
const getCFVVirtualPrice = function(code) {
  return getCFVault(contract[code].CFVault)
    .methods.get_virtual_price()
    .call()
}
//获取CFController地址
const getController = function(CFVault) {
  return getCFVault(CFVault)
    .methods.controller()
    .call()
}
//获取IUSDCPool地址
const getIUSDCPool = async function(CFVault) {
  let cfc = await getController(CFVault)
  return getWeb3(CFController_abi, cfc)
    .methods.current_pool()
    .call()
}
//获取IUSDCPoolname
const getIUSDCName = async function(code) {
  let iusd = await getIUSDCPool(contract[code].CFVault)
  return getWeb3(IUSDCPool_abi, iusd)
    .methods.name()
    .call()
}
//获取CFVbalanceOf
const getUSDCObj = function(address) {
  return getCFVault()
    .methods.get_virtual_price()
    .call()
}
export {
  getWeb3,
  getCFVault,
  getIUSDCName,
  getIERCBalanceOf,
  getCFVBalanceOf,
  getCFVVirtualPrice,
  setDeposit,
  getWithdraw,
  getAllowance,
  setApprove,
  getPadding,
  MintThenSwap,
  getCFTName,
  getMaxAMount,
  setDepositETH,
  getWithdrawETH,
  getBalance,
  getDy,
  getGoundage,
  getHarvestFeeRatio
}
