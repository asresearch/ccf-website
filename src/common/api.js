import axios from 'axios'
import { API_URL } from '../config.js'
const getAsset = (address) => {
  console.log(address,'address')
  return axios.get(`${API_URL}/getAsset`, {
    params: {
      address: address,
    },
  })
}
export { getAsset}
